import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, Injectable, InjectionToken, NgModule } from '@angular/core';
import {RouterModule,Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule as NgRxStoreModule, ActionReducerMap, Store } from '@ngrx/store';
import { EffectsModule} from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools'
 import {HttpClientModule, HttpHeaders, HttpRequest,HttpClient}  from  '@angular/common/http';
 import Dexie from 'dexie';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DestinoComponent } from './components/destino/destino.component';
import { ListaComponent } from './components/lista/lista.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import {NgxMapboxGLModule} from 'ngx-mapbox-gl';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


//import { DestinosApiClient } from './models/destinos-api-client.model';
import { TranslateService,TranslateLoader, TranslateModule } from '@ngx-translate/core';

import { FormDestinoComponent } from './components/form-destino/form-destino.component';
import { DestinosViajesState, reducerDestinosViajes,intializeDestinosViajesState, DestinosViajesEffects,InitMyDataAction } from './models/destinos-viajes-state.model';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { UsuarioLogueadoGuard } from './guards/usuario-logueago/usuario-logueado.guard';
import { AuthService } from './services/auth.service';

import { VuelosDetalleComponent } from './components/vuelos/vuelos-detalle/vuelos-detalle.component';
import { VuelosComponent } from './components/vuelos/vuelos/vuelos.component';
import { VuelosMasInfoComponent } from './components/vuelos/vuelos-mas-info/vuelos-mas-info.component';
import { VuelosMainComponent } from './components/vuelos/vuelos-main/vuelos-main.component';
import { ReservasModule } from './reservas/reservas.module';
import { DestinoViaje } from './models/destino-viaje.model';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { buildDriverProvider } from 'protractor/built/driverProviders';
import { from } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';
 export interface AppConfig {
   apiEndpoint: String;
 }
 const APP_CONFIG_VALUE: AppConfig ={
   apiEndpoint: 'http://localhost:3000'
 };
 export const APP_CONFIG = new InjectionToken<AppConfig> ('app.config');

export const childrenRoutesVuelos: Routes =[
  { path: '', redirectTo:'main', pathMatch:'full'},
  { path: 'main', component: VuelosMainComponent},
  { path: 'mas-info', component: VuelosMasInfoComponent},
  { path: ':id', component: VuelosDetalleComponent},

]

const routes:Routes =[
  {path:'', redirectTo:'home',pathMatch:'full'},
  {path:'home', component:ListaComponent},
  {path:'destino/:id', component:DestinoDetalleComponent},
  {path:'login', component:LoginComponent},
  {
    path: 'protected',
    component: ProtectedComponent,
    canActivate:[ UsuarioLogueadoGuard]
  },
  {
    path: "vuelos",
    component: VuelosComponent,
    canActivate:[UsuarioLogueadoGuard],
    children: childrenRoutesVuelos
  }

];
 
// REDUX INIT///
export interface AppState{
  destinos:DestinosViajesState;
}
 const reducers: ActionReducerMap<AppState> ={
   destinos:reducerDestinosViajes
 };

 const reducersInitialState ={
   destinos: intializeDestinosViajesState()
 }
// app init
  export function init_app(appLoadService: AppLoadService): () => Promise<any> {
    return () => appLoadService.intializeDestinosViajesState();
  }
   @Injectable()
   class AppLoadService{
     constructor(private store: Store<AppState>, private http: HttpClient) {}
     async intializeDestinosViajesState(): Promise<any> {
       const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': ' token-seguridad'});
       const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', { headers:headers});
       const response: any = await this.http.request(req).toPromise();
       this.store.dispatch(new InitMyDataAction(response.body));
     }
   }
//redux fin init

//dexie db
export class Translation {
   constructor ( public id: number, public lang: string, public key: string, public value: string){

   }
}
@Injectable({
  providedIn: 'root'
})
export class MyDatabase extends Dexie {
  destinos: Dexie.Table<DestinoViaje, number>;
  translations: Dexie.Table<Translation, number>;
  constructor(){
    super('MyDatabase');
    this.version(1).stores({
      destinos:'++id,nombre,descripcion'
    });
    this.version(2).stores({
      destinos:'++id,nombre,descripcion',
      translations: '++id, lang,key,value'
  });
  }
}
 export const db= new MyDatabase();
 //fin dexio

 // i18n ini
 class TranslationLoader implements TranslateLoader{
   constructor(private http: HttpClient){}

   getTranslation(lang: string): Observable<any>{
     const promise = db.translations
                    .where('lang')
                    .equals(lang)
                    .toArray()
                    .then(results =>{
                        if(results.length === 0){
                          return this.http
                          .get<Translation[]> (APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang='+ lang)
                          .toPromise()
                          .then(apiResults =>{
                            db.translations.bulkAdd(apiResults);
                            return apiResults;
                          });
                        }
                        return results;

                      }).then((traducciones) =>{
                        console.log('traduciones cargadas');
                        console.log(traducciones);
                        return traducciones;
                      }).then((traducciones) =>{
                        return traducciones.map((t) => ({ [t.key]: t.value}));
                      });

                      return from(promise).pipe(flatMap((elems) => from(elems)));
   }
 }

 function HttpLoaderFactory(http: HttpClient){
    return new TranslationLoader(http);
 }
@NgModule({
  declarations: [
    AppComponent,
    DestinoComponent,
    ListaComponent,
    DestinoDetalleComponent,
    FormDestinoComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosDetalleComponent,
    VuelosComponent,
    VuelosMasInfoComponent,
    VuelosMainComponent,
    EspiameDirective,
    TrackearClickDirective,


  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    //NgRxStoreModule.forRoot(reducers,{ initialState: reducersInitialState}),
    NgRxStoreModule.forRoot(reducers,{ initialState: reducersInitialState,
    runtimeChecks:{
      strictStateImmutability:false,
      strictActionImmutability:false,
    }
    }),
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument({

    }),
    ReservasModule,
    TranslateModule.forRoot({
      loader:{
        provide: TranslateLoader,
        useFactory:(HttpLoaderFactory),
        deps:[HttpClient]
      }
    }),
    NgxMapboxGLModule,
    BrowserAnimationsModule

 
  ],
  providers: [ 
 
     AuthService,UsuarioLogueadoGuard,
      {provide: APP_CONFIG, useValue:APP_CONFIG_VALUE},
      AppLoadService,
       { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true},
       MyDatabase
    ],
  
  bootstrap: [AppComponent]
})
export class AppModule { }


