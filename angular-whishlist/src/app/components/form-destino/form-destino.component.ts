import { Component, EventEmitter, forwardRef, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators,ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map,filter,debounceTime,distinctUntilChanged,switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { AppConfig, APP_CONFIG } from 'src/app/app.module';

@Component({
  selector: 'app-form-destino',
  templateUrl: './form-destino.component.html',
  styleUrls: ['./form-destino.component.css']
})
export class FormDestinoComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg:FormGroup;
  minLongitud=5;
  searchResults: string [];


  constructor(fb: FormBuilder, @Inject(forwardRef(()=> APP_CONFIG)) private config:AppConfig) {
    
    this.onItemAdded =new EventEmitter();

    this.fg =fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidator,
        this.nombreValidatorParametizable(this.minLongitud)
      ])],
    descripcion: ['']
    });


    this.fg.valueChanges.subscribe((form:any)=>{
      console.log('form cambio', form);
    });

    this.fg.controls['nombre'].valueChanges.subscribe((value:any)=>{
      console.log('nombre cambio', value);
    });
  }

  ngOnInit(){
    const elemNombre = <HTMLInputElement> document.getElementById('nombre');
    fromEvent(elemNombre, 'input')
    .pipe(
      map((e: KeyboardEvent)=> (e.target as HTMLInputElement).value),
      filter(text =>text.length > 2),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((text: string)=> ajax( this.config.apiEndpoint + '/ciudades?q=' + text))
    ).subscribe(ajaxResponse => this.searchResults = ajaxResponse.response);

      //switchMap(()=> ajax("/assets/datos.json"))
   // ).subscribe(ajaxResponse => {
      //***console.log(ajaxResponse);
      //***console.log(ajaxResponse.response);
      //this.searchResults = ajaxResponse.response
      //FILTRAMOS CLIENT SIDE
      //.filter(function(x){
       // return x.toLowerCase().includes(elemNombre.value.toLowerCase());
     // })

   // });
  }

 guardar(nombre:string, descripcion:string): boolean{
  let d = new DestinoViaje(nombre,descripcion);
  this.onItemAdded.emit(d);
  return false;
 }
 nombreValidator(control: FormControl): { [s:string]: boolean }{
   const l = control.value.toString().length;
   if(l>0 && l<5){
     return {invalidNombre: true}
   }
   return null;

 }

 nombreValidatorParametizable(minLong: number): ValidatorFn {
   return(control: FormControl): {[s:string]: boolean} | null => {
    const l = control.value.toString().length;
    if(l>0 && l< minLong){
      return {minLongNombre: true}
    }
     return null;
   } 
 }


}




